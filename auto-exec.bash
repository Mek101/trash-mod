#!/bin/bash
if [ "$EUID" -ne 0 ]; then
	echo 'Eseguire come root, o non eseguire!'
	exit -1
fi

echo 'Rimozione pacchetti superflui'
apt purge hypnotix sticky
apt autopurge

echo 'Aggiornamento del sistema'
apt update
apt upgrade

echo 'Installazione e configurazione pacchetti aggiuntivi'
apt install --no-install-recommends --no-install-recommends stress htop screenkey smartmontools micro xclip hdparm
systemctl disable smartmontools

echo 'Installazione wallpaper'
mkdir -p /usr/local/share/backgronds
cp wallpaper.png /usr/local/share/backgronds/wallpaper.png

echo 'Disabilitazione core dump'
mkdir -p /etc/systemd/coredump.conf.d/
cp coredump.conf /etc/systemd/coredump.conf.d/coredump.conf

echo 'Installazione tweaks memoria sysctl'
cp 50-vm-trashware.conf /etc/sysctl.d/50-vm-trashware.conf

echo 'Installazione regole udev per disco e memoria'
cp 10-hdparm.rules /etc/udev/rules.d/10-hdparm.rules
cp 60-io-scheduler.rules /etc/udev/rules.d/60-io-scheduler.rules

echo 'Installazione tweaks per TCP'
cp 80-net-trashware.rules /etc/udev/rules.d/80-net-trashware.rules

echo 'Installazione tweaks per risparmio energetico'
cp 91-disk-power-management.rules /etc/udev/rules.d/91-disk-power-management.rules
cp 91-pci-power-management.rules /etc/udev/rules.d/91-pci-power-management.rules

echo 'Installazione loading dei moduli aggiuntivi al montaggio della root'
cp bfq-trashware.conf /etc/modules-load.d/bfq-trashware.conf
cp tcp_bbr-trashware.conf /etc/modules-load.d/tcp_bbr-trashware.conf

echo 'Installazione configurazione zswamp per grub'
cp 50_zswap_trashware.cfg /etc/default/grub.d/50_zswap_trashware.cfg

echo "Rebuild dell'initramfs e grub"
update-initramfs -u
update-grub

echo 'Installazione limite dei log'
mkdir -p /etc/systemd/journald.conf.d
cp 50-max-use-trashware.conf /etc/systemd/journald.conf.d/50-max-use-trashware.conf

echo 'Intallazione di trashware-test'
cp trashware-test /usr/local/bin/trashware-test
chmod +x /usr/local/bin/trashware-test
